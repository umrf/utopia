# Rentrez query

* Automatic download based on entrez query. 

```bash
Rscript rentrez_q.R
```

# Merge files + extract taxonomy

```bash
# merge files
cat *fasta > 00_final/all_ITS_ncbi.fasta
cat query_tab_*.tsv > 00_final/all_ITS_ncbi_tab.tsv

#seq in one line
fasta_formatter -i 00_final/all_ITS_ncbi.fasta -o 00_final/all_ITS_ncbi_tidy.fasta -w 0

#filtering
prinseq-lite.pl -fasta sequences.fna -out_format 1 -out_good sequences_good -noniupac -ns_max_n 1 -min_len 100 -max_len 5000

# Dereplication vsearch
vsearch --derep_fulllength sequences_good.fasta --output derep_test.fasta

# Taxonomy
taxonkit lineage -j 2 --data-dir /home-local/rifa/bank/blast/taxdump/ --taxid-field 1 uniq_taxid.txt  > lineage.txt

taxonkit reformat -j 2 -f "{k};{p};{c};{o};{f};{g};{s}" -i 2 lineage.txt > lineage_formatted.txt

# Final adjustments
sed -i "s/$/;/g " lineage_formatted.txt
sed -i "s/'//g;s/#//g" lineage_formatted.txt
sed -i "s/'//g;s/#//g" all_ITS_ncbi_tab.tsv


```

# Subset dataset

* Select 50 sequences per taxon for identified sequences
* Keep uncultered fungus sequences

```r
library(Biostrings)
library(dplyr)

A = read.table("lineage_formatted.txt", sep="\t")
B = read.table("all_ITS_ncbi_tab.tsv", sep="\t")


A2 = TabF = dplyr::right_join(A, B, by = c("V1" = "V5"))
names(TabF) = c("taxid", "taxonomy", "taxonomy_7rank", "gi", "gd", "extra", "taxon")

write.table(TabF, "final_lineage_all_seq.tsv", quote = FALSE, row.names = FALSE, sep = "\t")

# A2 = read.table("final_lineage_all_seq.tsv", sep="\t", h = TRUE)
row.names(A2) = A2$gd

seq = readDNAStringSet("derep_test.fasta")
snames = names(seq)

l1 = strsplit(snames, "\\.")
newnames = sapply(l1, `[[`, 1)
names(seq) = newnames

A2good = A2[newnames,]

A3 = A2good[A2good[,7] != "uncultured fungus" | A2good[,7] != "uncultured fungus",]
A3_uncult = A2good[A2good[,7] == "uncultured fungus" | A2good[,7] == "uncultured fungus",]
A3_uncult <- droplevels(A3_uncult)

# Select 50 sequences per taxon.
A3 <- droplevels(A3)
subsetT = A3 %>% group_by(taxon) %>% slice(1:50) %>% ungroup()
tt = table(subsetT$taxon); max(tt); min(tt)

write.table(subsetT, "subset_table.tsv", row.names = FALSE, sep = "\t", quote = FALSE)

final_seq = seq[na.omit(subsetT$gd)]

uncult_seq = seq[na.omit(A3_uncult$gd)]

all_final_seq = c(final_seq, uncult_seq)

dir.create("subset_sequences")
writeXStringSet(final_seq, './subset_sequences/subset_taxon_sequence.fasta')
writeXStringSet(uncult_seq, './subset_sequences/uncult_sequence.fasta')
writeXStringSet(all_final_seq, './subset_sequences/subset_all_sequence.fasta')

all_seq_tab = rbind.data.frame(subsetT, A3_uncult)
write.table(all_seq_tab, "all_subset_table.tsv", row.names = FALSE, sep = "\t", quote = FALSE)

```

# CDHIT

* 0.9 identity clustering

```bash
# sbatch cluster genotoul SLURM
cd-hit -i subset_all_sequence.fasta -o subset_seq.cdhit_c1_s090 -c 1 -T 9 -s 0.90 -sf 1 -sc 1 -M 28000
```


# Generate final database

* Select taxonomy of remaining sequences after clustering step.
* Taxonomy table correction
* Output final database in TSV format
* Generate IDTAXA formatted database.

```R
library(ranomaly)

setwd("/home-local2/rifa/projets/anomaly/DBs/ITS/00_final/")
seqclust = readDNAStringSet("subset_sequences/cdhit_clustering_090_100/subset_seq.cdhit_c1_s090")
A2 = read.table("final_lineage_all_seq.tsv", sep="\t", h = TRUE, stringsAsFactors = FALSE)
row.names(A2) = A2$gd

taxclust = A2[names(seqclust),]

# Fill and Check taxonomy

tt = strsplit(taxclust$taxonomy_7rank, ";")

tt2 = t(sapply(tt, "["))
tt2[tt2 == ""] = NA

ttax = as.data.frame(tt2)

row.names(ttax) = taxclust$gd


#Fill tax table
filltable = fill_tax_fun(ttax, prefix = FALSE)

# #Check tax table
check1 = check_tax_fun(filltable, output = NULL, rank = 6)

all(row.names(check1) == row.names(ttable_ok))
seq = as.character(seqclust)

out_check1 = cbind( idnum = 1:nrow(check1), id = row.names(check1), check1, seq = seq[row.names(check1)])

write.table(out_check1, "UTOPIA_2021_checked_tax_idtaxa.tsv", quote= FALSE, sep ="\t", row.names = FALSE)

save.image(file = "utopia_build.rdata")

all(names(seqclust) == row.names(check1))

#Generate taxid
taxid <- taxid_fun(taxtable = check1, output = NULL)
head(taxid[taxid$Level == 1,])

#Train IDTAXA db

idtaxa_traindb(taxtable = check1, taxid = taxid, seqs = seqclust, outputDIR = "./", outputDBname = "UTOPIA2021_IDTAXA.rdata", returnval = FALSE)

# Test
idtaxa_assign_fasta_fun("rep-seqs_mercade_its2_100.fna", "../UTOPIA2021_IDTAXA.rdata")


# No unassigned / Eukaryota_phylum
dir.create("no_unassigned")
check2 = check1[check1$Phylum != "unassigned_phylum" & check1$Phylum != "Eukaryota_phylum", ]

out_check2 = cbind( idnum = 1:nrow(check2), id = row.names(check2), check2, seq = seq[row.names(check2)])


write.table(out_check2, "no_unassigned/UTOPIA_2021_checked_tax_idtaxa2.tsv", quote= FALSE, sep ="\t", row.names = FALSE)

# save.image(file = "utopia_build.rdata")

all(names(seqclust) == row.names(check1))

#Generate taxid
taxid <- taxid_fun(taxtable = check2, output = NULL)
head(taxid[taxid$Level == 1,])

#Train IDTAXA db

idtaxa_traindb(taxtable = check2, taxid = taxid, seqs = seqclust[row.names(check2)], outputDIR = "./no_unassigned", outputDBname = "UTOPIA2021_IDTAXA2.rdata", returnval = FALSE)

```
