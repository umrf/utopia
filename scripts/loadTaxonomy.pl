#!/usr/bin/perl
use strict ;
use File::Basename;
use lib '../lib';
use Data::Dumper;
use Getopt::Long;

use Logger;
use Taxonomy;
use DBI;
use SQL::SplitStatement;


my $taxo_struct_dmp = 'taxonomyStructure.sql';
my $data_nodes = 'nodes.dmp';
my $data_names = 'names.dmp';
my $dir = '.';
my $verbosity=1;


GetOptions(
  "names=s"  => \$data_names,
  "nodes=s"  => \$data_nodes,
  "struct=s" => \$taxo_struct_dmp,
  "dir=s"    => \$dir,
  "v=i"      => \$verbosity,
);

Logger::Logger->changeMode($verbosity);


&main;


sub main {
  my $self={};
  bless $self;
  _set_options($self);

  $self->{_sep} = { names	=> '\t\|\t|\t\|$',
                    nodes => '\t\|\t|\t\|$'
                  };

	#
  my $db_path = $dir . '/taxonomy.tmp.sqlite';
  _create_sqlite_db($self,$db_path);
  my $dbh = DBI->connect("dbi:SQLite:dbname=$db_path","","");
  $self->_insertingCSVDataInDatabase($dbh,$self->{_data});
  $dbh->disconnect;
}

sub _insertingCSVDataInDatabase {
	my ($self,$dbh,$tablesDataFiles) = @_;
  $logger->info('Inserting tables into database...');
	foreach my $table (keys %{$tablesDataFiles}){
    $logger->info($table);
		my $sth = $dbh->column_info( undef, undef, $table, '%');
		my $ref = $sth->fetchall_arrayref;
		my @cols = map { $_->[3] } @$ref;

		$logger->debug("Inserting data in table $table ...\n");
    $dbh->{AutoCommit} = 0;
    my $req = "INSERT OR IGNORE INTO $table ( ".join(',', map {"'".$_."'"} @cols)." ) VALUES (".join(',', map {'?'} @cols).")";
    $logger->debug($req);
		$sth = $dbh->prepare( $req ) or $logger->logdie($dbh->errstr);

		my $separator;
    if(defined $self->{_sep}->{$table}){
      $separator = $self->{_sep}->{$table};
    }

    foreach my $file (@{$tablesDataFiles->{$table}}){
      open (DATA, $file) || $logger->logdie($file);
      while (<DATA>) {
        chomp;
        $sth->execute(grep {$_ !~ /^$separator$/} split (/($separator)/, $_)) or $logger->logdie($dbh->errstr);
      }
      close DATA;
    }

		$dbh->commit or $logger->logdie($dbh->errstr);
		$logger->debug("Insertion of data in table $table finished\n");
	}
}


sub _create_sqlite_db {
  my ($self,$file) = @_;
  $logger->info('Creating database.');
  if(-e $file){
    `mv $file $file.'_old'`;
  }
	if(! -e $file){
		`touch $file`;
		my $dbh = DBI->connect("dbi:SQLite:dbname=$file","","");
		_executeSQLFiles($self,$dbh,($self->{_taxo_struct_dmp}));
		$dbh->disconnect;
	}
	else{
		$logger->warn('Database already exists. Skip...')
	}
}


sub _executeSQLFiles {
	my ($self,$dbh,@sqlFiles) = @_;
	my $sql_splitter = SQL::SplitStatement->new;
	foreach my $file (@sqlFiles){
    $logger->debug('Reading sql file:' . $file);
    my $cmd;
		open (FILE, $file) or $logger->logdie("Unable to open the SQL file : $file\n");
		while( <FILE> ){
      $cmd.= $_;
    }
		close FILE;
    my @statements = $sql_splitter->split($cmd);
		foreach (@statements){
      $logger->debug('Executing sql cmd:');
      $logger->debug($_);
			$dbh-> do($_) or $logger->logdie($dbh->errstr);
		}
	}
}


sub _set_options {
  my ($self)=@_;
  if(-e $taxo_struct_dmp){
    $self->{_taxo_struct_dmp} = $taxo_struct_dmp;
  }
  else{
    $logger->error($taxo_struct_dmp . ' taxo_struct_dmp file not found.');
    &help;
  }
  if(-e $data_nodes){
    push(@{$self->{_data}->{nodes}},$data_nodes);
  }
  else{
    $logger->error($data_nodes . ' data_nodes file not found.');
    &help;
  }
  if(-e $data_names){
    push(@{$self->{_data}->{names}},$data_names);
  }
  else{
    $logger->error($data_names . ' data_names file not found.');
    &help;
  }
}


sub help {
my $prog = basename($0);
print STDERR <<EOF ;
#### $prog ####
#
# AUTHOR:     Sebastien THEIL
# LAST MODIF: 19/09/2015
# PURPOSE:    This script is used to load NCBI taxonomy file into a SQLite database.

USAGE:
      $prog  -struct taxonomyStructure.sql -index taxonomyIndex.sql -acc_prot prot.accession2taxid -acc_nucl nucl.accession2taxid -names names.dmp -nodes nodes.dmp

			### OPTIONS ###
      -struct     <path>   taxonomyStructure.sql path. (Default: $taxo_struct_dmp)
      -names      <path>   names.dmp file. (Default: $data_names)
      -nodes      <path>   nodes.dmp file. (Default: $data_nodes)
      -v          <int>      Verbosity level. (0 -> 4).
EOF
exit(1);
}
