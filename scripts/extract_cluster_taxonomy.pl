#!/usr/bin/perl
use strict;
use File::Basename;
use Data::Dumper;
use lib '/home/stheil/git-repo/itsbank/lib';
use Taxonomy;
use Fasta;
use Getopt::Long;

my $taxo_file='';
my $clstr_file='';
my $fasta_file='';
my $mode='';
my $dir='./';
my $threshold=0.8;
my $prefix='';
my $verbosity=1;

GetOptions(
  "taxo=s"      => \$taxo_file,
  "clstr=s"     => \$clstr_file,
	"fasta=s"     => \$fasta_file,
	"dir=s"       => \$dir,
	"mode=s"      => \$mode,
	"threshold=s" => \$threshold,
	"prefix=s"    => \$prefix,
  "v=i"         => \$verbosity,
);

&main;


sub main {
  my $self={};
  bless $self;
  _set_options($self);
	if(! -e $dir){
		mkdir $dir;
	}
	$self->_read_taxonomy($taxo_file);
	$self->_read_cdhit_clstr($clstr_file);
	$self->_compute_clstr();
	$self->_print_outputs();
	$self->_print_stats();
  $self->_print_clstr_taxo();
	# print Dumper $self->{valid_clstr};
}



sub _read_taxonomy{
	my ($self,$file)=@_;
	open(TAXO,$file);
	while(<TAXO>){
		chomp;
		my @line = split("\t",$_);
		$self->{taxo}->{$line[0]} = $line[1];
	}
	close TAXO;
}



sub _read_cdhit_clstr{
	my ($self,$file)=@_;
	my $cluster_id;
	my $data_clstr;
	my $ref_seq;
	open(CLSTR,$file);
	while(<CLSTR>){
		chomp;
		if(/^>Cluster\s(\d+)$/){
			$cluster_id = $1;
			next;
		}
		if(/^\d+\s+\d+aa,\s>(\S+)\.\.\.\sat\s\d+\.\d+%$/){
			$self->{stats}->{nb_seq}++;
			$self->{data_clstr}->{$cluster_id}->{$1} = $self->{taxo}->{$1};
		}
		elsif(/^\d+\s+\d+aa,\s>(\S+)\.\.\.\s\*$/){
			$self->{data_clstr}->{$cluster_id}->{$1} = $self->{taxo}->{$1};
			$self->{ref_seq}->{$cluster_id} = $1;
			$self->{stats}->{nb_seq}++;
		}
	}
	close CLSTR;
}



sub _print_clstr_taxo{
	my ($self)=@_;
	open(FILE,">".$dir."/".$prefix."_cluster_taxo.txt");
  foreach my $lvl (sort(keys(%{$self->{valid_clstr}}))){
    foreach my $cluster_id (keys(%{$self->{valid_clstr}->{$lvl}})){
      print FILE $lvl . "\t" . '***' . "\t" . $self->{valid_clstr}->{$lvl}->{$cluster_id} . "\n";
      foreach my $id (keys(%{$self->{data_clstr}->{$cluster_id}})){
        print FILE $lvl . "\t" . $cluster_id . "\t" . $id . "\t" . $self->{data_clstr}->{$cluster_id}->{$id} . "\n";
      }
    }
  }
	close FILE;
}



sub _compute_clstr{
	my ($self)=@_;
CLSTR:	foreach my $clstr (keys(%{$self->{data_clstr}})){
		my $lvl=6;
		while($lvl >= 0){
			my ($response,$str) = _calc_percent($self->{data_clstr}->{$clstr},$lvl);
			if($response == 1){
				$self->{valid_clstr}->{$lvl}->{$clstr} = $str;
				last;
			}
			elsif($lvl == 0 && $response == 0){
				print STDERR 'No rank convergence at given threshold.' . "\n";
				print STDERR Dumper $self->{data_clstr}->{$clstr};
				next CLSTR;
			}
			else{
				$lvl--;
			}
		}
	}
}



sub _print_stats{
	my ($self)=@_;
	my $total=0;
	print 'nb_seq in cdhit file: ' . $self->{stats}->{nb_seq} . "\n";
	print 'level'."\t".'clusters'."\t".'sequences'."\n";
	foreach my $lvl (sort(keys(%{$self->{valid_clstr}}))){
		my $sum_seq=0;
		foreach my $cluster_id (keys(%{$self->{valid_clstr}->{$lvl}})){
			$sum_seq+= scalar(keys(%{$self->{data_clstr}->{$cluster_id}}));
		}
		$total+=$sum_seq;
		print $lvl . "\t" . scalar(keys(%{$self->{valid_clstr}->{$lvl}})) . "\t" . $sum_seq . "\n";
	}
	print 'total: '. $total . "\n";
	print 'nb_printed: ' . $self->{stats}->{nb_printed} . "\n";
	print 'nb_poor_annotation: ' . $self->{stats}->{poor_annot}."\n";
}



sub _print_outputs{
	my ($self)=@_;
	my $seq_fasta = Fasta->new(file => $fasta_file);
	open(TAX,'>'.$dir.'/'.$prefix.'_taxonomy.txt') || die 'Unable to open TAX in write mode.';
	open(FASTA,'>'.$dir.'/'.$prefix.'_sequences.fna') || die 'Unable to open FASTA in write mode.';
	foreach my $lvl (sort(keys(%{$self->{valid_clstr}}))){
		foreach my $cluster_id (keys(%{$self->{valid_clstr}->{$lvl}})){
			if($lvl == 6){
				if($self->{valid_clstr}->{$lvl}->{$cluster_id} =~ /p__Fungi_phylum/){
					$self->{stats}->{poor_annot}++;
					next;
				}
				$self->{stats}->{nb_printed}++;
				print TAX $self->{ref_seq}->{$cluster_id} . "\t" . $self->{valid_clstr}->{$lvl}->{$cluster_id} . "\n";
				print FASTA $seq_fasta->retrieveFastaBlock($self->{ref_seq}->{$cluster_id});
			}
			else{
				if($mode eq 'split'){
					foreach my $id (keys(%{$self->{data_clstr}->{$cluster_id}})){
						if($self->{data_clstr}->{$cluster_id}->{$id} =~ /p__Fungi_phylum/){
							$self->{stats}->{poor_annot}++;
							next;
						}
						$self->{stats}->{nb_printed}++;
						print TAX $id . "\t" . $self->{data_clstr}->{$cluster_id}->{$id} . "\n";
						print FASTA $seq_fasta->retrieveFastaBlock($id);
					}
				}
				elsif($mode eq 'reannotate'){
					my @taxo = split(';',$self->{valid_clstr}->{$lvl}->{$cluster_id});
					my $y;
					foreach my $x (@taxo){
						push(@{$y},{'name' => $x});
					}
					my $tool = Taxonomy->new();
					my $taxo_string = $tool->_getTaxonomyString($y);
					if($taxo_string =~ /p__Fungi_phylum/){
						$self->{stats}->{poor_annot}++;
						next;
					}
					$self->{stats}->{nb_printed}++;
					print TAX $self->{ref_seq}->{$cluster_id} . "\t" . $tool->_getTaxonomyString($y) . "\n";
					print FASTA $seq_fasta->retrieveFastaBlock($self->{ref_seq}->{$cluster_id});
				}
			}
		}
	}
	close FASTA;
	close TAX;
}



sub _calc_percent {
	my ($data,$lvl)= @_;
	my $taxo_string;
	my $sum=0;
	foreach my $id (keys(%{$data})){
		my @s = split(';',$data->{$id});
		my $str = join(';',@s[ 0 .. $lvl ]);
		if($str=~/s__uncultured_fungus/ || $str=~/p__Fungi_phylum/ || $str=~/c__Fungi_class/ || $str=~/o__Fungi_order/ || $str=~/f__Fungi_family/ || $str=~/g__Fungi_genus/){
			next;
		}
		$taxo_string->{$str}++;
		$sum++;
	}

	my $taxo_percent;
	foreach my $str (keys(%{$taxo_string})){
		$taxo_percent->{$str} = $taxo_string->{$str}/$sum;
	}

	foreach my $str (keys(%{$taxo_percent})){
		if($taxo_percent->{$str} >= $threshold){
			return (1,$str);
		}
	}
	return (0,'');
}



sub _set_options {
  my ($self)=@_;
	if(! -e $taxo_file){
		print STDERR "You must provide a taxonomy file." . "\n";
		&help()
	}
	if(! -e $clstr_file){
		print STDERR "You must provide a CD-HIT cluster file." . "\n";
		&help();
	}
}


sub help {
my $prog = basename($0);
print STDERR <<EOF ;
#### $prog ####
#
# AUTHOR:     Sebastien THEIL
# LAST MODIF: 10/01/2019
# PURPOSE:    This script is used to extract sequences and taxonomy from CD-HIT clustering. Two modes are proposed : split confilcted clusters or reannotate representative sequence at last homogenous taxonomic rank.

USAGE:
      $prog  -taxo taxonomy.txt -clstr sequences_cdhit.clstr -dir out_dir -mode [split/reannotate]

       ### OPTIONS ###
      -taxo       <path>   Taxonomy file. Two column TSV file: seqID\\t7taxonomyrank.
      -clstr      <path>   names.dmp file.
      -mode       <path>   Mode to resolve conflict cluster.
      -dir        <path>   Output directory. (Default: $dir)
      -threshold  <float>  0 -> 1. Threshold to determine homogenous taxonomic rank. (Default: $threshold)
      -v          <int>    Verbosity level. (0 -> 4).
EOF
exit(1);
}
