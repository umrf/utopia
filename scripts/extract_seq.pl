#!/usr/bin/perl
use strict;
use Bio::SeqIO;
use lib '/home/stheil/git-repo/itsbank/lib';
use Taxonomy;
use Data::Dumper;
use DBI;

if(defined($ARGV[0])){
	if(! -e $ARGV[0]){
		print STDERR 'File not found ' . $ARGV[0] . '.' . "\n";
		exit(1);
	}
}
else{
	print STDERR 'You must provide a GenBank file.' . "\n";
	exit(1);
}

my $db = '/home/db/taxonomy/taxonomy.tmp.sqlite';

my $embl_file = Bio::SeqIO->new(-file => $ARGV[0], -format => 'genbank');

my $out_fasta = open(FASTA,">sequences.fna");
my $out_taxo = open(TAXO,">taxonomy.txt");

my %taxonomyParams;
$taxonomyParams{'dbh'} = DBI->connect("dbi:SQLite:dbname=$db","","");
$taxonomyParams{_dbFormat} = 'reduced';
my $taxo_tool = Taxonomy->new(%taxonomyParams);


while(my $seq = $embl_file->next_seq){
	foreach my $feat ($seq->get_SeqFeatures()){
		if($feat->primary_tag() eq 'source'){
			if($feat->has_tag('db_xref')){
				my @tag_values = $feat->get_tag_values('db_xref');
				foreach my $tag (@tag_values){
					if($tag =~ /^taxon:(\d+)$/){
						my $taxid = $1;
						print FASTA '>' . $seq->accession_number() . "\n";
						print FASTA $seq->seq() . "\n";
						print TAXO $seq->accession_number() . "\t" . $taxo_tool->taxId2taxonomy($taxid) . "\n";
					}
				}
			}
		}
	}
}
close FASTA;
close TAXO;
